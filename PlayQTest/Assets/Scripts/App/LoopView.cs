﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

public class LoopView : EditorWindow
{
    private GUISkin _customSkin;
    private Container _container;
    private List<bool> _viewData;

    private Vector2 _scrollPosition;

    private float _viewportHeight;
    private Vector2 _elementHeight;

    private int _viewQty = 4;
    private int _nodesQty = 11;

    [MenuItem("Window/Loop View")]
    static void ShowWindow()
    {
        LoopView window = EditorWindow.GetWindow<LoopView>("Loop View", true);
        window.Show();
    }

    private void Awake()
    {
        //Load custom skin to decorate our layout properly
        _customSkin = Resources.Load<GUISkin>("ScrollViewSkin");

        //Measure element height to create proper qty of views
        var elementStyle = _customSkin.FindStyle("greenbox");
        _elementHeight = new Vector2(0, elementStyle.fixedHeight + elementStyle.margin.bottom);

        //Let;s create initial data
        CreateInitialData();
    }

    private void OnGUI()
    {
        GUI.skin = _customSkin;

        using (var verticalLayout = new EditorGUILayout.VerticalScope())
        {
            //Check viewport height to add additional view if default value will not be enough
            _viewportHeight = verticalLayout.rect.height;

            using (var scrollView = new EditorGUILayout.ScrollViewScope(_scrollPosition))
            {
                //Move scroll to proper position
                Move(scrollView.scrollPosition);

                for (var i = 0; i < _viewData.Count; i++)
                {
                    GUILayout.Box(_viewData[i].ToString(),
                    _viewData[i] ? GUI.skin.FindStyle("greenbox") : GUI.skin.FindStyle("redbox"));
                }
            }
            if (GUILayout.Button("Create new"))
            {
                CreateInitialData();
            }
        }

        //Check whether all viewport is covered with views
        CheckFreeSpace();
    }

    /// <summary>
    /// Creates data to fill views
    /// </summary>
    private void CreateInitialData()
    {
        _container = new Container(_nodesQty);
        _viewData = new List<bool>();

        _container.MoveBackward();
        _viewData.Add(_container.Value);

        //Create view data according to the number of views
        for (var i = 0; i < _viewQty; i++)
        {
            _container.MoveForward();
            _viewData.Add(_container.Value);
        }
    }

    /// <summary>
    /// Checks wether free space is available.
    /// If there is some add data to cover it with views
    /// </summary>
    private void CheckFreeSpace()
    {
        var viewQty = (int)Mathf.Ceil(_viewportHeight / _elementHeight.y);

        if(viewQty > _viewQty)
        {
            for(var i = 0; i < viewQty - _viewQty; i++)
            {
                _viewData.Add(_container.Value);
                _container.MoveForward();
            }

            _viewQty = viewQty;
        }
    }

    /// <summary>
    /// Moves scroll view to proper position
    /// </summary>
    /// <param name="scrollPosition">Current postion if scroll view</param>
    private void Move(Vector2 scrollPosition)
    {
        if (scrollPosition.y - _elementHeight.y >= _elementHeight.y)
        {
            _scrollPosition = _elementHeight;

            _viewData.Add(_container.Value);
            _viewData.RemoveAt(0);

            _container.MoveForward();

            return;
        }
        else if (_elementHeight.y - scrollPosition.y >= _elementHeight.y)
        {
            _scrollPosition = _elementHeight;

            _viewData.Insert(0, _container.Value);
            _viewData.RemoveAt(_viewData.Count - 1);

            _container.MoveBackward();

            return;
        }

        _scrollPosition = scrollPosition;
    }

}
