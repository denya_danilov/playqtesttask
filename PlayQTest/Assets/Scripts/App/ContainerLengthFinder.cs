﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.App
{
    public class ContainerLengthFinder
    {
        private readonly Container _container;

        //Due to the fact that node value is randomized and
        //probability of getting sequence longer than 50 values is very low
        //let's limit logic of sequence (000000, 0101010101, 001001001001) check by this value
        private readonly int _sequenceThreshold;

        public ContainerLengthFinder(Container container, int threshold = 50)
        {
            _container = container;
            _sequenceThreshold = threshold;
        }

        /// <summary>
        /// Runs script
        /// </summary>
        /// <param name="nodesNumber">Number of nodes to create</param>
        /// <returns>number of nodes that were count</returns>
        public int Run(int nodesNumber)
        {
            //Algorithm is based on the assumption that value property
            //of the container points on the first element of the list
            var nodesCount = CountNodes();

            Debug.LogFormat("Container has {0} nodes", nodesCount);

            return nodesCount;
        }

        /// <summary>
        /// Implement root logic of algorithm
        /// </summary>
        /// <returns>Number of nodes</returns>
        private int CountNodes()
        {
            var sameFirstLast = FirstAndLastSameValue();

            //Let's check that container has 1 node only
            if (sameFirstLast && HasOneNode())
            {
                return 1;
            }

            int nodesCount = 0;

            //Container has more than 1 node let's count them
            DetermineNodesSequence(sameFirstLast, ref nodesCount);
            Debug.LogFormat("nodesCount long: {0}", nodesCount);
            
            return nodesCount;
        }

        /// <summary>
        /// Checks if the first and the last nodes have the same value
        /// </summary>
        /// <returns>True if nodes have the same value</returns>
        private bool FirstAndLastSameValue()
        {
            //We are on the first node
            var firstNodeValue = _container.Value;
            //Let's move to the last and check it's value
            _container.MoveBackward();
            var lastNodeValue = _container.Value;

            var hasSameValues = firstNodeValue == lastNodeValue;

            //We need to move to the first node,
            //because we have 2 nodes and HasOneNode will not be invoked
            if (!hasSameValues)
            {
                _container.MoveForward();
            }

            return hasSameValues;
        }

        /// <summary>
        /// Checks if list has only one value
        /// </summary>
        /// <returns>True if list has the only value</returns>
        private bool HasOneNode()
        {
            //Changing value of the last node
            _container.Value = !_container.Value;
            var lastNodeValue = _container.Value;
            //Moving to the first node
            _container.MoveForward();
            var firstNodeValue = _container.Value;
            //Restoring value of the last node
            _container.MoveBackward();
            _container.Value = !_container.Value;
            //Returning to the first node
            _container.MoveForward();

            return lastNodeValue == firstNodeValue;
        }

        /// <summary>
        /// Finds values sequence and counts it's length
        /// </summary>
        /// <param name="sameFirstLast">Describes whether the first and the last noed have the same value</param>
        /// <param name="count">Describes number of nodes that were count</param>
        private void DetermineNodesSequence(bool sameFirstLast, ref int count)
        {
            //StringBuilder will be used as a container of the nodes values
            var sequence = new List<byte>();
            //Let's use this value to control our position in the list
            var positionOffset = 0;

            //Search of the values sequence is based on the assumption that after
            //containers pointer will pass two loops we will have 2 equal sequences
            //of values as the members of the string
            bool found = false;

            do
            {
                sequence.Add(_container.Value ? (byte)1 : (byte)0);
                _container.MoveForward();
                positionOffset++;

                //To make the first check we need to have even qty of string members
                //to be able to devide it into 2 sequnces with the same length
                if (sequence.Count % 2 == 0 && count < sequence.Count / 2)
                {

                    //Let's check whether all elements of the sequences are equal to
                    //each other positionwise
                    found = true;

                    count = sequence.Count / 2;

                    for(var i=0; i<count; i++)
                    {
                        if(sequence[i] != sequence[count + i])
                        {
                            found = false;
                        }
                    }

                    //Let't also check that found sequence suits our previous results
                    found = found && (sameFirstLast && sequence[0] == sequence[count - 1] ||
                                     !sameFirstLast && sequence[0] != sequence[count - 1]);

                    //Now we need to check that the end of the sequence is the end of the list
                    //To do this let's move to the end of the list and add the third sequence to the string
                    //and then compare the second and the third sequence
                    //We need to do this because range of nodes values can contain same sequences inside
                    //For example: 01010101

                    if (found && count < _sequenceThreshold)
                    {
                        //Move to the last node
                        for (var i = 0; i <= positionOffset; i++)
                        {
                            _container.MoveBackward();
                        }

                        var oppositeValue = !_container.Value;

                        //Let's rewrite node values to break the sequence
                        //We will get loop of the same values if the sequence covers the loop
                        for (var i = 1; i <= count; i++)
                        {
                            sequence.Add(_container.Value ? (byte)1 : (byte)0);
                            _container.Value = oppositeValue;
                            _container.MoveBackward();
                        }

                        //Let's move to the first element
                        MakeForwardSteps(count + 1);

                        //Let's check that the sequence from the first element was rewritten
                        //if not we need to continue our search
                        for (var i = 1; i <= count; i++)
                        {
                            if (_container.Value != oppositeValue)
                            {
                                found = false;
                            }
                            _container.MoveForward();
                        }

                        //We need to restore rewritten values now
                        //Let's move to the first element
                        MakeBackwardSteps(count);

                        //Let's restore original values of the nodes
                        for (var i = 0; i < count; i++)
                        {
                            _container.MoveBackward();
                            var charIndex = sequence.Count - count + i;
                            var originalValue = sequence[charIndex];
                            _container.Value = (originalValue == 1) ? true : false;
                            sequence.RemoveAt(charIndex);
                        }

                        //Move back to the point where we have paused search (positionOffset)
                        MakeForwardSteps(count + positionOffset);

                    }


                    if (found)
                    {
                        Debug.LogFormat("Long sequence: {0}", sequence.ToString());
                    }

                }

            } while (!found);

        }

        private void MakeForwardSteps(int stepsNum)
        {
            for (var i = 1; i <= stepsNum; i++)
            {
                _container.MoveForward();
            }
        }

        private void MakeBackwardSteps(int stepsNum)
        {
            for (var i = 1; i <= stepsNum; i++)
            {
                _container.MoveBackward();
            }
        }
    }
}
