﻿using NUnit.Framework;
using Assets.Scripts.App;

namespace Tests
{
    public class AlgorithmTest
    {
        // A Test behaves as an ordinary method
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(5)]
        [TestCase(12)]
        [TestCase(50)]
        [TestCase(253)]
        [TestCase(1001)]
        public void ContainerLengthFinderTest(int nodesNumber)
        {
            var container = new Container(nodesNumber);
            var finder = new ContainerLengthFinder(container);
            var result = finder.Run(nodesNumber);

            Assert.AreEqual(nodesNumber, result);

        }

        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-200)]
        public void TestWithNegativeValues(int nodesNumber)
        {
            var container = new Container(nodesNumber);
            var finder = new ContainerLengthFinder(container);
            var result = finder.Run(nodesNumber);

            Assert.True(result>0);
        }

    }
}
